const server = require('../');
const chai = require('chai');

const expect = chai.expect;
chai.use(require('chai-http'));

describe('server', () => {
  it('responts to the /a endpoint', () => {
    chai.request(server)
      .get('/a')
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.req).to.be.json;
        expect(res.body).to.be.deep.equal({
          a: 'we are in package a, but this is module b',
        });
      });
  });
});
