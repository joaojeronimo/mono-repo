const express = require('express');
const a = require('a');

const app = express();

app.get('/a', (req, res) => res.json({ a }));

module.exports = app;
