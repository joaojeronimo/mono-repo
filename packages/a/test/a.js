const a = require('../');
const chai = require('chai');

const expect = chai.expect;

describe('a', () => {
  it('is a', () => {
    expect(a).to.be.equal('we are in package a, but this is package b');
  });
});
