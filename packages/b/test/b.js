const b = require('../');
const chai = require('chai');

const expect = chai.expect;

describe('b', () => {
  it('is b', () => {
    expect(b).to.be.equal('this is package b');
  });
});
